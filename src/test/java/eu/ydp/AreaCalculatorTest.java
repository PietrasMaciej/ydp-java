package eu.ydp;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.ydp.domain.Circle;
import eu.ydp.domain.Rectangle;
import eu.ydp.domain.Square;

public class AreaCalculatorTest {
	
	Square square = new Square();
	Rectangle rectangle = new Rectangle();
	Circle circle = new Circle();
	
	@Test
	public void checkSquareArea() {
		 assertTrue(36 == square.area(6));
		 
		 assertEquals(0, square.area(0), 0);
	}
	
	@Test
	public void checkRectangleArea() {
		 assertEquals(5, rectangle.area(1, 5), 0.01);
		 assertEquals(4.9284, rectangle.area(2.22, 2.22), 0.0001);
		 assertEquals(44, rectangle.area(0.22, 200), 0.01);
	}
	
	@Test
	public void checkCircleArea() {
		assertEquals(3.14, circle.area(1), 0.01);
	}
	
	@Test
	public void regexTest() {
		assertNotNull(circle.figureParameters("circle", 0));
		
		assertEquals("(?i)^(area\\ssquare\\s(\\d+|0\\.\\d+|\\d+\\.\\d+))$", square.figureParameters("square", 1));
		assertEquals("(?i)^(area\\srectangle\\s(\\d+|0\\.\\d+|\\d+\\.\\d+)\\s(\\d+|0\\.\\d+|\\d+\\.\\d+))$", rectangle.figureParameters("rectangle", 2));
	}
	
	@Test
	public void oneParamTest() {
		assertEquals(0.22, square.getParamIfOne("area square 0.22"), 0.01);
		assertEquals(244, square.getParamIfOne("area square 244"), 0.01);
		
		assertNotEquals(-1, square.getParamIfOne("area square 1"), 0);
	}
	
	@Test
	public void twoParamsTest() {
		double arguments[] = rectangle.getParamsIfTwo("area rectangle 0.1 255");
		
		assertEquals(0.1, arguments[0], 0.0);
		assertEquals(255, arguments[1], 0.0);

	}

}
