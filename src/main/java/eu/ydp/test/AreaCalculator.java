package eu.ydp.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import eu.ydp.domain.Circle;
import eu.ydp.domain.Figure;
import eu.ydp.domain.Rectangle;
import eu.ydp.domain.Square;

public class AreaCalculator {

    public static void main(String[] args) {

    	Square square = new Square();
    	Figure rectangle = new Rectangle();
    	Circle circle = new Circle();
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	Date date = new Date();
    	
        boolean ok = true;
        
        Scanner s = new Scanner(System.in);
        
        ArrayList<String> cache = new ArrayList<String>();

        while (ok) {
        	
            System.out.print("calculator > ");
            String commandString = s.nextLine();

            System.out.println("Command: " + commandString);
            
            
            if (commandString.equalsIgnoreCase("exit")) {          	
            	ok = false;
            }           

            if (commandString.matches(square.figureParameters("square", 1))) {
            	double area = square.area(square.getParamIfOne(commandString));
            	System.out.println(area);
            	cache.add(dateFormat.format(date)+ ": " + "square " + area);
            }
            
            if (commandString.matches(rectangle.figureParameters("rectangle", 2))) {
            	double arguments[] = rectangle.getParamsIfTwo(commandString);
            	double area = rectangle.area(arguments[0], arguments[1]);
            	System.out.println(area);
            	cache.add(dateFormat.format(date)+ ": " + "rectangle " + area);	
            }
            
            if (commandString.matches(circle.figureParameters("circle", 1))) {

            	double area = circle.area(circle.getParamIfOne(commandString));
            	System.out.println(area);
            	cache.add(dateFormat.format(date)+ ": " + "circle " + area);
            }
            
            if (commandString.equalsIgnoreCase("showall")) {
            	for(int i = 0; i < cache.size(); i++) {   
            	    System.out.print(cache.get(i) + "\n");
            	}  
            }
            
            if (commandString.equalsIgnoreCase("hello")) { 
            	    System.out.print("Welcome to Area Calculator. Enter command.\n"); 
            }
            
        }
        s.close();
    }
}
