package eu.ydp.domain;

public class Circle extends Figure {

	public double area(double r) {
		return area(Math.PI * r * r, 1);
	}
	
}
