package eu.ydp.domain;
import org.apache.commons.lang.StringUtils;

public abstract class Figure {
	
	private float a;
	private float b;
	

	public float getA() {
		return a;
	}
	
	public void setA(float a) {
		this.a = a;
	}
	
	public float getB() {
		return b;
	}
	
	public void setB(float b) {
		this.b = b;
	}
	
	public double area(double a, double b) {
		return a*b;
		
	}
	
	public String figureParameters(String figure, int paramNumber) {
		String parameterRegex = "\\s(\\d+|0\\.\\d+|\\d+\\.\\d+)";
		if (paramNumber > 1)
			return "(?i)^(area\\s" + figure + StringUtils.repeat(parameterRegex, paramNumber) + ")$";
		else
			return "(?i)^(area\\s" + figure + "\\s(\\d+|0\\.\\d+|\\d+\\.\\d+))$";
		
	}
	
	public double getParamIfOne(String commandString) {
		int from = commandString.lastIndexOf(" ")+1;
    	int to = commandString.length();
    	return Double.parseDouble(commandString.substring(from,to));
	}
	
	public double[] getParamsIfTwo(String commandString) {
		int fromA = (StringUtils.ordinalIndexOf(commandString, " ", 2))+1;
    	int toA = commandString.lastIndexOf(" ");
		int fromB = commandString.lastIndexOf(" ")+1;
    	int toB = commandString.length();
    	double a = Double.parseDouble(commandString.substring(fromA, toA));
    	double b = Double.parseDouble(commandString.substring(fromB, toB));
    	return new double[] {a, b};
	}

}
